const puppeteer = require('puppeteer');
const path = require('path');

async function generatePDF() {
    const browser = await puppeteer.launch({
        devtools: false,
        args: ['--no-sandbox',]
    });
    const page = await browser.newPage();
    await page.goto(`file:${path.join(__dirname, 'book.html')}`)
    await page.waitForNetworkIdle()

    await page.evaluate((env) => {
        let year = document.querySelector('#year');
        year.innerText = year.innerText.replace(/{{year}}/g, new Date().getFullYear());
        const build = document.querySelector('#build')
        build.innerText = build.innerText.replace(/{{build}}/g, new Date().getTime());
        const userName = document.querySelector('#employee_name')
        userName.innerText = env.EMPLOYEE_NAME ? ` ${env.EMPLOYEE_NAME}` : '';
    }, process.env);

    await page.pdf({
        path: `book.pdf`,
        printBackground: true,
        format: 'A5',
    });

    await browser.close();
}

generatePDF()
